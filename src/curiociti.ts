import { BloomskyService } from './lib/BloomskyShotter';
import * as fs from 'fs';
import * as _ from 'lodash';
import * as axios from 'axios';
import Bluebird from 'bluebird';
const cron = require('cron').CronJob;

const url =
  'https://us-central1-greengrass-q.cloudfunctions.net/prod-api/photos';

const ownerAccountId = '27446558';
const ownerId = 'LvapHhrerHta9C6xo2HI';
const ownerAlias = 'komunidad';
const reportType = 'ReportType.TYPHOON';
const subReportType = 'TyphoonType.INFO';
const ownerPhoto =
  'https://c.curiociti.io/profilePics/LvapHhrerHta9C6xo2HI/thumb_scaled_image_picker8129227283323501506.jpg';

async function upload(
  path: string,
  address: string,
  caption: string,
  pos: any,
) {
  try {
    const contents = await fs.promises.readFile(path, {
      encoding: 'base64',
    });

    const photo = {
      ownerId,
      ownerAccountId,
      photo: contents,
      reportType,
      subReportType,
      ownerAlias,
      isVideo: false,
      anonymous: false,
      caption: caption,
      address: address,
      ownerPhoto,
      lat: pos.lat,
      lng: pos.lng,
      skipNotification: true,
    };

    const { data } = await axios.default.post(url, photo);

    return data;
  } catch (e) {
    throw e;
  }
}

const shotter = new BloomskyService({
  apiKey: 's-e0tpfZreHZdd2av6u_48uRzd3IsQ==',
  concurrency: 5,
  dir: './tmp/curiociti/',
  id: 'bloomsky',
  period: 60,
  whitelist: [],
});

async function poll() {
  const images = await shotter.getImages();
  shotter.photoMaxAge = 1;
  Bluebird.map(
    images,
    async (image) => {
      const path = await shotter.processImage(image);
      if (path) {
        const raining = image.Data.Rain ? 'Raining' : '';
        const caption = `${raining}
temperature: ${Math.round(image.Data.Temperature)}
humidity: ${Math.round(image.Data.Humidity)}`;

        const photo = await upload(path, image.FullAddress, caption, {
          lat: image.LAT,
          lng: image.LON,
        });

        console.log('created ', photo);
      }
    },
    {
      concurrency: 1,
    },
  );
}

function run() {
  console.log('running job');
  const job = new cron(
    '0 */15 * * * *',
    async () => {
      await poll();
    },
    null,
    true,
    'Asia/Manila',
  );
  job.start();
}
run();
