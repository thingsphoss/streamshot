import * as fs from 'fs';

import StreamWorker, { StreamJob } from './lib/StreamWorker';
import * as _ from 'lodash';
import { FtpConfig } from './lib/Ftp';

import BloomskyWorker, { BloomskyJob } from './lib/BloomskyWorker';

export enum ConfigurationType {
  RTSP = 'RTSP',
  BLOOMSKY = 'BLOOMSKY',
}

export interface Configuration {
  enabled: boolean;
  type: ConfigurationType;
  id: string;
  concurrency: number;
  dryRun: boolean;
  period: number;
  ftp: FtpConfig;
  options: any;
  cron: string;
}

fs.promises
  .readFile('./config.json')
  .then((configStr) => {
    const config: Configuration[] = JSON.parse(configStr.toString());
    console.log(`loaded ${config.length} configuration`);
    _.map(config, (conf) => {
      if (!conf.enabled) {
        return;
      }
      if (conf.type === ConfigurationType.RTSP) {
        const opts: StreamJob = {
          id: conf.id,
          concurrency: conf.concurrency,
          dryRun: conf.dryRun,
          cameras: conf.options.cameras,
          ftp: conf.ftp,
          cron: conf.cron,
        };
        const worker = new StreamWorker(opts);
        worker.poll();
      } else if (conf.type === ConfigurationType.BLOOMSKY) {
        const opts: BloomskyJob = {
          dryRun: conf.dryRun,
          ftp: conf.ftp,
          concurrency: conf.concurrency,
          id: conf.id,
          options: conf.options,
          cron: conf.cron,
        };
        const worker = new BloomskyWorker(opts);
        worker.poll();
      }
    });
  })
  .catch((e) => {
    console.log('err', e);
  });
