const Client = require('ftp');

export interface FtpConfig {
  host: string;
  port: number;
  user: string;
  password: string;
}
export class Ftp {
  config: FtpConfig;
  client: any;
  constructor(config: FtpConfig) {
    this.config = config;
  }
  async makedir(dir) {
    return new Promise<void>((resolve, reject) => {
      const c = new Client();
      c.on('ready', function () {
        c.mkdir(dir, function (err) {
          if (err) {
            reject(err);
          } else {
            // c.end();
            resolve();
          }
        });
      });
      c.on('error', (e) => {
        console.log(`error while making directory ${e.message}`);
      });
      c.connect(this.config);
    });
  }

  async upload(src, dest) {
    return new Promise<void>((resolve, reject) => {
      const c = new Client();
      c.on('ready', function () {
        c.put(src, dest, function (err) {
          if (err) {
            reject(err);
          } else {
            // c.end();
            resolve();
          }
        });
      });
      c.on('error', (e) => {
        console.log(`error while uploading ${e.message}`);
      });
      c.connect(this.config);
    });
  }
}
