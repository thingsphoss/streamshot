require('dotenv').config();
import * as Axios from 'axios';
import Bluebird from 'bluebird';
import * as uuid from 'uuid';
import moment from 'moment-timezone';
import * as fs from 'fs';
const tz = 'Asia/Manila';

export type ResultItem = {
  UTC: number;
  CityName: string;
  Storm: {};
  Searchable: true;
  DeviceName: string;
  RegisterTime: number;
  DST: number;
  BoundedPoint: '';
  LON: number;
  Point: {};
  VideoList: string[];
  VideoList_C: string[];
  DeviceID: string;
  NumOfFollowers: number;
  LAT: number;
  ALT: number;
  Data: {
    Luminance: number;
    Temperature: number;
    ImageURL: string;
    TS: number;
    Rain: boolean;
    Humidity: number;
    Pressure: number;
    DeviceType: string;
    Voltage: number;
    Night: number;
    UVIndex: number;
    ImageTS: number;
  };
  FullAddress: string;
  StreetName: string;
  PreviewImageList: string[];
};

export interface BloomskyConfig {
  id: string;
  apiKey: string;
  concurrency: number;
  period: number;
  whitelist: string[];
  dir: string;
}

export class BloomskyService {
  config: BloomskyConfig;
  photoMaxAge = 1;
  constructor(config: BloomskyConfig) {
    this.config = config;
  }

  async getImages(): Promise<ResultItem[]> {
    try {
      const endpoint =
        'https://api.bloomsky.com/api/skydata/?unit=intl&DeviceID=442C05FF6865';
      const result = await Axios.default.get(endpoint, {
        headers: {
          authorization: this.config.apiKey,
        },
      });
      return result.data;
    } catch (e) {
      console.log('faile', e.message);
      return [];
    }
  }

  async mkdir() {
    try {
      await fs.promises.mkdir(`./tmp/bloomsky/`);
    } catch (e) {
      // console.log(e);
    }
  }
  async pullAndPush(): Promise<string[]> {
    await this.mkdir();
    const result = await this.getImages();
    const images = await Bluebird.map(
      result,
      async (i: ResultItem) => {
        return await this.processImage(i);
      },
      { concurrency: 5 },
    );
    return images;
  }

  async processImage(item: ResultItem): Promise<string> {
    const t = moment().tz(tz);
    const imageDate = moment(item.Data.ImageTS * 1000).tz(tz);
    const filename = `${this.config.dir}/${item.DeviceID}-${uuid.v4()}.jpg`;
    const diff = t.diff(imageDate, 'hours');

    if (diff >= this.photoMaxAge) {
      console.log('skippinng');
      return '';
    }

    if (this.config.whitelist && this.config.whitelist.length > 0) {
      if (!this.config.whitelist.includes(item.DeviceID)) {
        return '';
      }
    }

    const response = await Axios.default.get(item.Data.ImageURL, {
      responseType: 'arraybuffer',
    });

    const image = Buffer.from(response.data);
    await fs.promises.writeFile(filename, image);
    return filename;
  }
}
