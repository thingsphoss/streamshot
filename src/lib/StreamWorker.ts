import * as fs from 'fs';
import * as path from 'path';
import Bluebird from 'bluebird';
import { Ftp, FtpConfig } from './Ftp';
import StreamShotter from './StreamShotter';
const cron = require('cron').CronJob;

export interface StreamResource {
  name: string;
  url: string;
}
export interface StreamJob {
  id: string;
  concurrency: number;
  dryRun: boolean;
  ftp: FtpConfig;
  cameras: StreamResource[];
  cron: string;
}

export default class StreamWorker {
  job: StreamJob;
  ftp: Ftp;

  constructor(job: StreamJob) {
    this.job = job;
    this.ftp = new Ftp(job.ftp);
  }

  async unlink(filename) {
    try {
      await fs.promises.unlink(filename);
    } catch (e) {}
  }

  async mkdir() {
    try {
      await fs.promises.mkdir(`./tmp/${this.job.id}`);
    } catch (e) {
      // console.log(e);
    }
  }

  async ftpMkdir() {
    try {
      await this.ftp.makedir(`/${this.job.ftp.user}`);
    } catch (e) {
      // console.log(e);
    }
  }

  async processStream() {
    const shotter = new StreamShotter();
    const config = this.job;
    const concurrency = config.concurrency || 2;
    console.log(
      `${this.job.id} processing with concurrency: ${concurrency} START`,
    );
    await this.mkdir();
    await this.ftpMkdir();

    const d = new Date();
    await Bluebird.map(
      config.cameras,
      async (camera) => {
        const filename = `./tmp/${this.job.id}/${camera.name}.jpg`;
        const ftpDest = `/${this.job.ftp.user}/${path.basename(filename)}`;
        try {
          console.log(`${this.job.id} camera: ${camera.name} PROCESSING`);
          await this.unlink(filename);
          await shotter.shot(camera.url, filename);
          if (!this.job.dryRun) {
            console.log(
              `${this.job.id} camera: ${camera.name} UPLOADING ${ftpDest}`,
            );
            await this.ftp.upload(filename, ftpDest);
          }
          console.log(`${this.job.id} camera: ${camera.name} DONE`);
        } catch (e) {
          console.log(
            `${this.job.id} camera: ${camera.name}: FAILED ${e.message}`,
          );
        }
      },
      {
        concurrency: concurrency,
      },
    );
    const ts = (new Date().getTime() - d.getTime()) / 1000;
    console.log(`${this.job.id} done in ${ts}s`);
  }

  poll() {
    console.log(`starting job ${this.job.id} ${this.job.cron}`);
    const job = new cron(
      this.job.cron,
      async () => {
        await this.processStream()
          .then(() => {
            console.log(`${this.job.id} done polling`);
          })
          .catch((e) => {
            console.log(`error processing job, backing off ${e.message}`);
          });
      },
      null,
      true,
      'Asia/Manila',
    );
    job.start();
  }
}
