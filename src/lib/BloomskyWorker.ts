import * as fs from 'fs';
import * as path from 'path';
import Bluebird from 'bluebird';
import { Ftp, FtpConfig } from './Ftp';
import { BloomskyConfig, BloomskyService } from './BloomskyShotter';
const cron = require('cron').CronJob;

export interface BloomskyJob {
  id: string;
  concurrency: number;
  dryRun: boolean;
  ftp: FtpConfig;
  options: BloomskyConfig;
  cron: string;
}

export default class BloomskyWorker {
  job: BloomskyJob;
  ftp: Ftp;

  constructor(job: BloomskyJob) {
    this.job = job;
    this.ftp = new Ftp(job.ftp);
  }

  async unlink(filename) {
    try {
      await fs.promises.unlink(filename);
    } catch (e) {}
  }

  async mkdir() {
    try {
      await fs.promises.mkdir(`./tmp/${this.job.id}`);
    } catch (e) {
      // console.log(e);
    }
  }

  async ftpMkdir() {
    try {
      await this.ftp.makedir(`/${this.job.ftp.user}`);
    } catch (e) {
      // console.log(e);
    }
  }

  async processStream() {
    const opts = this.job.options;
    opts.dir = path.resolve(`./tmp/${this.job.id}/`);

    const bloomsky = new BloomskyService(opts);
    console.log('job options', this.job.options);
    const config = this.job;
    const concurrency = config.concurrency || 2;
    console.log(
      `${this.job.id} processing with concurrency: ${concurrency} START`,
    );
    await this.mkdir();
    await this.ftpMkdir();

    const d = new Date();
    const images = bloomsky.pullAndPush();
    await Bluebird.map(
      images,
      async (image) => {
        if (image) {
          const ftpDest = `/${this.job.ftp.user}/${path.basename(image)}`;
          try {
            console.log(`${this.job.id} camera: ${image} PROCESSING`);

            if (!this.job.dryRun) {
              console.log(
                `${this.job.id} camera: ${image} UPLOADING ${ftpDest}`,
              );
              await this.ftp.upload(image, ftpDest);
            }
            console.log(`${this.job.id} camera: ${image} DONE`);
          } catch (e) {
            console.log(`${this.job.id} camera: ${image}: FAILED ${e.message}`);
          } finally {
            await fs.promises.unlink(image);
          }
        }
      },
      {
        concurrency: concurrency,
      },
    );
    const ts = (new Date().getTime() - d.getTime()) / 1000;
    console.log(`${this.job.id} done in ${ts}s`);
  }

  poll() {
    console.log(`starting job ${this.job.id} ${this.job.cron}`);
    const job = new cron(
      this.job.cron,
      async () => {
        await this.processStream()
          .then(() => {
            console.log(`${this.job.id} done polling`);
          })
          .catch((e) => {
            console.log(`error processing job, backing off ${e.message}`);
          });
      },
      null,
      true,
      'Asia/Manila',
    );
    job.start();
  }
}
