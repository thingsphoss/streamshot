const ffmpeg = require('fluent-ffmpeg');
export default class StreamShotter {
  shot(url: string, filename: string): Promise<void> {
    return new Promise((resolve, reject) => {
      ffmpeg(url, {
        logger: console,
      })
        .inputOptions(['-rtsp_transport tcp', '-y'])
        .outputOptions('-vframes 1')
        .save(filename)
        .on('end', () => {
          resolve();
        })
        .on('error', (e) => {
          reject(e);
        });
    });
  }
}
