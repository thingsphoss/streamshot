# build
FROM ubuntu
RUN apt-get -y update
RUN apt-get install -y curl ffmpeg
ADD . /src
WORKDIR /src/
SHELL ["/bin/bash", "-c"]
ENV BASH_ENV ~/.bashrc
ENV VOLTA_HOME /root/.volta
ENV PATH $VOLTA_HOME/bin:$PATH
RUN curl https://get.volta.sh | bash
RUN volta install node
RUN npm install
RUN npm run build
RUN mkdir tmp
CMD ["node", "dist/index.js"]