#### STARTING THE STREAMSHOTTER

1. Create your own config (`config.json`). See `sample.config.json` for sample configuration options.
2. Start the streamshotter. The command below starts the container with a custom config.json

 `$ docker run  -d --restart unless-stopped   -v /full/path/to/config.json:/src/config.json public.ecr.aws/h3s8m5l0/streamshot`

