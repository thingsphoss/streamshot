#!/bin/bash
aws ecr-public get-login-password --region us-east-1 | docker login --username AWS --password-stdin public.ecr.aws
docker push  public.ecr.aws/h3s8m5l0/streamshot:latest